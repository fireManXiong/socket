package socket

import (
	"crypto/tls"
	"net"
	"net/http"
	"sync"
	"testing"
	"time"

	"github.com/gorilla/websocket"
)

func TestSocketTCP(t *testing.T) {
	server := NewTCPServer(TCPServerConfig{
		ReadBufferSize: 1024 * 1024,
		TLS: &tls.Config{
			Certificates: []tls.Certificate{},
		},
	})
	wait := sync.WaitGroup{}
	wait.Add(10)
	server.OnEvent(1, func(conn net.Conn, data []byte) {
		server.Emit(conn, 2, []byte("Hello,Client!"))
	})
	err := server.Listen("tcp", "127.0.0.1:8080")
	if err != nil {
		t.Error(err)
	}
	client := NewTCPClient(TCPClientConfig{
		ReadBufferSize: 1024 * 1024,
		TLS: &tls.Config{
			InsecureSkipVerify: true,
		},
	})
	client.OnOpen(func(conn net.Conn) {
		go func() {
			for i := 0; i < 10; i++ {
				client.Emit(conn, 1, []byte("Hello,Server!"))
			}
		}()
	})
	client.OnEvent(2, func(conn net.Conn, data []byte) {
		wait.Done()
	})
	client.Dial("tcp", "127.0.0.1:8080")
	done := make(chan bool)
	go func() {
		<-time.After(time.Second * 2)
		done <- true
	}()
	go func() {
		wait.Wait()
		done <- true
	}()
	select {
	case <-done:
	}
}

func TestSocketWS(t *testing.T) {
	server := NewWSServer(WSServerConfig{
		Upgrader: &websocket.Upgrader{
			ReadBufferSize:  1024 * 1024,
			WriteBufferSize: 1024 * 1024,
			CheckOrigin:     func(r *http.Request) bool { return true },
		},
	})
	wait := sync.WaitGroup{}
	wait.Add(10000)

	server.OnEvent(1, func(conn *websocket.Conn, data []byte) {
		server.Emit(conn, 2, []byte("Hello,Client!"))

	})
	server.Listen("127.0.0.1:8080", "/")
	client := NewWSClient()
	client.OnOpen(func(conn *websocket.Conn) {
		go func() {
			for i := 0; i < 10000; i++ {
				client.Emit(conn, 1, []byte("Hello,Server!"))
			}
		}()
	})
	client.OnEvent(2, func(conn *websocket.Conn, data []byte) {
		wait.Done()
	})
	client.Dial("ws://127.0.0.1:8080/", nil)
	done := make(chan bool)
	go func() {
		<-time.After(time.Second * 2)
		done <- true
	}()
	go func() {
		wait.Wait()
		done <- true
	}()
	select {
	case <-done:
	}
}

func TestS(t *testing.T) {

}
