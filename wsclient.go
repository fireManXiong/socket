package socket

import (
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

type WSClientConfig struct {
	CallMaxWaitTime time.Duration
}

var default_ws_client_config = WSClientConfig{
	CallMaxWaitTime: 10 * time.Second,
}

type WSClient interface {
	Dial(address string, requestHeader http.Header) error
	OnOpen(f func(conn *websocket.Conn))
	OnClose(f func(conn *websocket.Conn))
	OnEvent(id int32, f func(conn *websocket.Conn, data []byte))
	Emit(conn *websocket.Conn, id int32, data []byte)
	GetConn() *websocket.Conn
	IsClosed() bool
}

func NewWSClient(config ...WSClientConfig) WSClient {
	if len(config) == 0 {
		config = append(config, default_ws_client_config)
	}
	return &wsclient{
		config: config[0],
		events: make(map[int32]func(conn *websocket.Conn, data []byte)),
	}
}

type wsclient struct {
	sync.RWMutex
	config   WSClientConfig
	onopen   func(conn *websocket.Conn)
	onclose  func(conn *websocket.Conn)
	events   map[int32]func(conn *websocket.Conn, data []byte)
	conn     *websocket.Conn
	isclosed bool
}

func (w *wsclient) Dial(address string, requestHeader http.Header) error {
	conn, _, err := websocket.DefaultDialer.Dial(address, requestHeader)
	if err != nil {
		return err
	}
	go func() {
		w.conn = conn
		if w.onopen != nil {
			w.onopen(conn)
		}
		defer func() {
			if w.onclose != nil {
				w.onclose(conn)
			}
			conn.Close()
			w.isclosed = true
		}()
		for {
			msgType, data, err := conn.ReadMessage()
			if err != nil {
				break
			}
			if msgType != websocket.BinaryMessage {
				break
			}
			msgs := unpack(data)
			for _, msg := range msgs {
				if w.events[msg.id] != nil {
					w.events[msg.id](conn, msg.data)
				}
			}
		}
	}()
	return nil
}

func (w *wsclient) OnOpen(f func(conn *websocket.Conn)) {
	w.onopen = f
}

func (w *wsclient) OnClose(f func(conn *websocket.Conn)) {
	w.onclose = f
}

func (w *wsclient) OnEvent(id int32, f func(conn *websocket.Conn, data []byte)) {
	w.events[id] = f
}

func (w *wsclient) Emit(conn *websocket.Conn, id int32, data []byte) {
	w.Lock()
	defer w.Unlock()
	db := pack(msg{
		id:   id,
		data: data,
	})
	conn.WriteMessage(websocket.BinaryMessage, db)
}

func (w *wsclient) GetConn() *websocket.Conn {
	return w.conn
}

func (w *wsclient) IsClosed() bool {
	return w.isclosed
}
