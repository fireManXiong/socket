# socket

#### 介绍
socket扩展包

#### 软件架构
软件架构说明


#### 安装教程

1.  go get gitee.com/fireManXiong/socket
2.  xxxx
3.  xxxx

#### 使用说明


1.  tcp
```go
package main

import (
	"fmt"
	"net"
	"sync"
	"time"

	"gitee.com/fireManXiong/socket"
)

func main() {
	server := socket.NewTCPServer(socket.TCPServerConfig{
		ReadBufferSize: 1024 * 1024,
	})
	wait := sync.WaitGroup{}
	wait.Add(4000)
	server.Listen("tcp", "127.0.0.1:8080")
	server.OnOpen(func(conn net.Conn) {
		fmt.Println("New connection:", conn.RemoteAddr().String())
		go func() {
			for i := 0; i < 2000; i++ {
				server.Emit(conn, socket.Pack(socket.Msg{
					ID:   2,
					Data: []byte("Hello,Client!"),
				}))
			}
		}()
		go func() {
			for i := 0; i < 2000; i++ {
				server.Emit(conn, socket.Pack(socket.Msg{
					ID:   2,
					Data: []byte("Hello,Client!"),
				}))
			}
		}()
	})
	client := socket.NewTCPClient()

	client.OnEvent(func(conn net.Conn, data []byte) {
		for _, msg := range socket.UnPack(data) {
			fmt.Println("Received data from", conn.RemoteAddr().String(), ":", msg.ID, string(msg.Data))
			wait.Done()
		}
	})
	client.Dial("tcp", "127.0.0.1:8080")
	done := make(chan bool)
	go func() {
		<-time.After(time.Second * 1)
		done <- true
	}()
	go func() {
		wait.Wait()
		done <- true
	}()
	select {
	case <-done:
	}
}
```
2.  ws
```go
package main

import (
	"fmt"
	"net/http"
	"sync"
	"time"

	"gitee.com/fireManXiong/socket"
	"github.com/gorilla/websocket"
)

func main() {
	server := socket.NewWSServer(socket.WSServerConfig{
		Upgrader: &websocket.Upgrader{
			ReadBufferSize:  1024 * 1024,
			WriteBufferSize: 1024 * 1024,
			CheckOrigin:     func(r *http.Request) bool { return true },
		},
		// OnHandler: func(res http.ResponseWriter, req *http.Request) bool {
		// 	return true
		// },
	})
	wait := sync.WaitGroup{}
	wait.Add(4000)
	server.OnOpen(func(conn *websocket.Conn) {
		fmt.Println("New connection:", conn.RemoteAddr().String())
		go func() {
			for i := 0; i < 2000; i++ {
				server.Emit(conn, socket.Pack(socket.Msg{
					ID:   2,
					Data: []byte("Hello,Client!"),
				}))
			}
		}()
		go func() {
			for i := 0; i < 2000; i++ {
				server.Emit(conn, socket.Pack(socket.Msg{
					ID:   2,
					Data: []byte("Hello,Client!"),
				}))
			}
		}()
	})
	server.Listen("127.0.0.1:8080", "/")
	client := socket.NewWSClient()
	client.OnEvent(func(conn *websocket.Conn, data []byte) {
		for _, msg := range socket.UnPack(data) {
			fmt.Println("Received data from", conn.RemoteAddr().String(), ":", msg.ID, string(msg.Data))
			wait.Done()
		}
	})
	client.Dial("ws://127.0.0.1:8080/", nil)
	done := make(chan bool)
	go func() {
		<-time.After(time.Second * 1)
		done <- true
	}()
	go func() {
		wait.Wait()
		done <- true
	}()
	select {
	case <-done:
	}
}

```
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
