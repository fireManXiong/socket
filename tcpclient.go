package socket

import (
	"crypto/tls"
	"net"
	"sync"
	"time"
)

type TCPClientConfig struct {
	ReadBufferSize  int
	CallMaxWaitTime time.Duration
	TLS             *tls.Config
}

var default_tcp_client_config = TCPClientConfig{
	ReadBufferSize:  1024,
	CallMaxWaitTime: 10 * time.Second,
}

type TCPClient interface {
	Dial(network, address string) error
	OnEvent(id int32, f func(conn net.Conn, data []byte))
	OnOpen(func(conn net.Conn))
	OnClose(func(conn net.Conn))
	Emit(conn net.Conn, id int32, data []byte)
}
type tcpclient struct {
	sync.RWMutex
	config  TCPClientConfig
	onopen  func(conn net.Conn)
	onclose func(conn net.Conn)
	events  map[int32]func(conn net.Conn, data []byte)
}

func NewTCPClient(config ...TCPClientConfig) TCPClient {
	if len(config) == 0 {
		config = append(config, default_tcp_client_config)
	}
	config_first := config[0]
	if config_first.ReadBufferSize <= 0 {
		config_first.ReadBufferSize = default_tcp_client_config.ReadBufferSize
	}
	if config_first.CallMaxWaitTime <= 0 {
		config_first.CallMaxWaitTime = default_tcp_client_config.CallMaxWaitTime
	}
	return &tcpclient{
		config: config_first,
		events: make(map[int32]func(conn net.Conn, data []byte)),
	}
}

func (t *tcpclient) Dial(network, address string) error {
	var conn net.Conn
	var err error
	if t.config.TLS != nil {
		conn, err = tls.Dial(network, address, t.config.TLS)
	} else {
		conn, err = net.Dial(network, address)
	}
	if err != nil {
		return err
	}
	go func() {
		buf := make([]byte, t.config.ReadBufferSize)
		defer func() {
			if t.onclose != nil {
				t.onclose(conn)
			}
			conn.Close()
		}()
		if t.onopen != nil {
			t.onopen(conn)
		}
		for {
			n, err := conn.Read(buf)
			if err != nil {
				break
			}
			data := buf[:n]
			msgs := unpack(data)
			for _, msg := range msgs {
				if t.events[msg.id] != nil {
					t.events[msg.id](conn, msg.data)
				}
			}
		}
	}()
	return err
}

func (t *tcpclient) OnOpen(f func(conn net.Conn)) {
	t.onopen = f
}

func (t *tcpclient) OnClose(f func(conn net.Conn)) {
	t.onclose = f
}

func (t *tcpclient) OnEvent(id int32, f func(conn net.Conn, data []byte)) {
	t.events[id] = f
}

func (t *tcpclient) Emit(conn net.Conn, id int32, data []byte) {
	t.Lock()
	defer t.Unlock()
	db := pack(msg{
		id:   id,
		data: data,
	})
	conn.Write(db)
}
