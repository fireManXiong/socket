package socket

import (
	"encoding/binary"
	"fmt"
)

type msg struct {
	id   int32
	data []byte
}

func pack(msg msg) []byte {
	idb := make([]byte, 4)
	lb := make([]byte, 2)
	binary.BigEndian.PutUint32(idb, uint32(msg.id))
	binary.BigEndian.PutUint16(lb, uint16(len(msg.data)))
	b := append(idb, lb...)
	b = append(b, msg.data...)
	return b
}

func unpack(data []byte) []msg {
	if len(data) < 6 {
		fmt.Println("解包数据长度不足6位")
		return nil
	}
	msgs := make([]msg, 0)
	for {
		id := int32(binary.BigEndian.Uint32(data[:4]))
		l := int(binary.BigEndian.Uint16(data[4:6]))
		if len(data) < 6+l {
			fmt.Println("解包数据有误")
			return msgs
		}
		msg := msg{id: id, data: data[6 : 6+l]}
		msgs = append(msgs, msg)
		data = data[6+l:]
		if len(data) < 6 {
			return msgs
		}
	}
}
