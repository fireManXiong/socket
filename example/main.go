package main

import (
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"strconv"
	"sync"
	"time"

	"gitee.com/fireManXiong/socket"
	"github.com/gorilla/websocket"
)

func main() {
	//TestSocketTCP()
	// TestSocketWS()
	TestSocketWSLogin()
}

func TestSocketTCP() {
	server := socket.NewTCPServer(socket.TCPServerConfig{
		ReadBufferSize: 1024 * 1024,
	})
	wait := sync.WaitGroup{}
	wait.Add(10000)
	server.OnEvent(1, func(conn net.Conn, data []byte) {
		server.Emit(conn, 2, []byte("Hello,Client!"))
	})
	server.Listen("tcp", "127.0.0.1:8080")
	client := socket.NewTCPClient(socket.TCPClientConfig{
		ReadBufferSize: 1024 * 1024,
	})
	client.OnOpen(func(conn net.Conn) {
		go func() {
			for i := 0; i < 10000; i++ {
				client.Emit(conn, 1, []byte("Hello,Server!"))
			}
		}()
	})
	client.OnEvent(2, func(conn net.Conn, data []byte) {
		wait.Done()
	})
	client.Dial("tcp", "127.0.0.1:8080")
	done := make(chan bool)
	go func() {
		<-time.After(time.Second * 2)
		done <- true
	}()
	go func() {
		wait.Wait()
		done <- true
	}()
	select {
	case <-done:
	}
}

func TestSocketWS() {
	server := socket.NewWSServer(socket.WSServerConfig{
		Upgrader: &websocket.Upgrader{
			ReadBufferSize:  1024 * 1024,
			WriteBufferSize: 1024 * 1024,
			CheckOrigin:     func(r *http.Request) bool { return true },
		},
	})
	wait := sync.WaitGroup{}
	wait.Add(10000)

	server.OnEvent(1, func(conn *websocket.Conn, data []byte) {
		server.Emit(conn, 2, []byte("Hello,Client!"))
	})
	server.Listen("127.0.0.1:8080", "/")
	client := socket.NewWSClient()
	client.OnOpen(func(conn *websocket.Conn) {
		go func() {
			for i := 0; i < 10000; i++ {
				client.Emit(conn, 1, []byte("Hello,Server!"))
			}
		}()
	})
	client.OnEvent(2, func(conn *websocket.Conn, data []byte) {
		wait.Done()
	})
	client.Dial("ws://127.0.0.1:8080/", nil)
	done := make(chan bool)
	go func() {
		<-time.After(time.Second * 2)
		done <- true
	}()
	go func() {
		wait.Wait()
		done <- true
	}()
	select {
	case <-done:
	}
}

const C2S_UserLogin_Id int32 = 1
const S2C_UserLogin_Id int32 = 2

func Marshal(data interface{}) []byte {
	jsonData, _ := json.Marshal(data)
	return jsonData
}

func Unmarshal(data []byte, v interface{}) {
	json.Unmarshal(data, v)
}

type C2S_UserLogin struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
type S2C_UserLogin struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func TestSocketWSLogin() {
	server := socket.NewWSServer(socket.WSServerConfig{
		Upgrader: &websocket.Upgrader{
			ReadBufferSize:  1024 * 1024,
			WriteBufferSize: 1024 * 1024,
			CheckOrigin:     func(r *http.Request) bool { return true },
		},
	})
	wait := sync.WaitGroup{}
	wait.Add(20)
	server.OnEvent(C2S_UserLogin_Id, func(conn *websocket.Conn, data []byte) {
		req := &C2S_UserLogin{}
		Unmarshal(data, req)
		server.Emit(conn, S2C_UserLogin_Id, Marshal(S2C_UserLogin{
			ID:   req.ID,
			Name: req.Name,
		}))
	})
	server.Listen("127.0.0.1:8080", "/")
	client := socket.NewWSClient()
	client.OnOpen(func(conn *websocket.Conn) {
		go func() {
			for i := 1; i <= 10; i++ {
				client.Emit(conn, C2S_UserLogin_Id, Marshal(C2S_UserLogin{
					ID:   i,
					Name: "client:" + strconv.Itoa(i),
				}))
			}
		}()
		go func() {
			for i := 11; i <= 20; i++ {
				client.Emit(conn, C2S_UserLogin_Id, Marshal(C2S_UserLogin{
					ID:   i,
					Name: "client:" + strconv.Itoa(i),
				}))
			}
		}()
	})
	client.OnEvent(S2C_UserLogin_Id, func(conn *websocket.Conn, data []byte) {
		req := S2C_UserLogin{}
		Unmarshal(data, &req)
		fmt.Println("登录成功:", req)
		wait.Done()
	})
	client.Dial("ws://127.0.0.1:8080/", nil)
	done := make(chan bool)
	go func() {
		<-time.After(time.Second * 2)
		done <- true
	}()
	go func() {
		wait.Wait()
		done <- true
	}()
	select {
	case <-done:
	}
}
