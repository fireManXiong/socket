package tcp

import "net"

type TCPServerConfig struct {
	ReadBufferSize int
}
type TCPServer struct {
	config TCPServerConfig
	onread func(conn net.Conn, data []byte)
}

var DefaultTCPServerConfig = TCPServerConfig{
	ReadBufferSize: 1024,
}

func NewTCPServer(config ...TCPServerConfig) *TCPServer {
	if len(config) == 0 {
		config = append(config, DefaultTCPServerConfig)
	}
	return &TCPServer{config: config[0]}
}

func (t *TCPServer) Listen(network, address string) (net.Listener, error) {
	lis, err := net.Listen(network, address)
	if err != nil {
		return nil, err
	}
	go func() {
		for {
			conn, err := lis.Accept()
			if err != nil {
				break
			}
			go func() {
				buf := make([]byte, t.config.ReadBufferSize)
				defer conn.Close()
				// handle connection
				for {
					// read data from connection
					n, err := conn.Read(buf)
					if err != nil {
						break
					}
					t.onread(conn, buf[:n])
				}
			}()
		}
	}()
	return lis, nil
}
func (t *TCPServer) OnRead(onread func(conn net.Conn, data []byte)) {
	t.onread = onread
}
func (t *TCPServer) Write(conn net.Conn, data []byte) {
	conn.Write(data)
}
