package socket

import (
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

type WSServerConfig struct {
	Upgrader  *websocket.Upgrader
	OnHandler func(res http.ResponseWriter, req *http.Request) bool
}

var default_ws_server_config = WSServerConfig{
	Upgrader: &websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	},
}

type WSServer interface {
	Listen(mAddr, cAddr string) error
	OnOpen(f func(conn *websocket.Conn, req *http.Request))
	OnClose(f func(conn *websocket.Conn))
	OnEvent(id int32, f func(conn *websocket.Conn, data []byte))
	Emit(conn *websocket.Conn, id int32, data []byte)
}

func NewWSServer(config ...WSServerConfig) WSServer {
	if len(config) == 0 {
		config = append(config, default_ws_server_config)
	}
	return &wsserver{
		config: config[0],
		events: make(map[int32]func(conn *websocket.Conn, data []byte)),
	}
}

type wsserver struct {
	sync.RWMutex
	config  WSServerConfig
	onopen  func(conn *websocket.Conn, req *http.Request)
	onclose func(conn *websocket.Conn)
	events  map[int32]func(conn *websocket.Conn, data []byte)
}

func (w *wsserver) handler(res http.ResponseWriter, req *http.Request) {
	if w.config.OnHandler != nil {
		valid := w.config.OnHandler(res, req)
		if !valid {
			return
		}
	}
	conn, err := w.config.Upgrader.Upgrade(res, req, nil)
	if err != nil {
		return
	}
	go func() {
		if w.onopen != nil {
			w.onopen(conn, req)
		}
		defer func() {
			if w.onclose != nil {
				w.onclose(conn)
			}
			conn.Close()
		}()
		for {
			msgType, data, err := conn.ReadMessage()
			if err != nil {
				break
			}
			if msgType != websocket.BinaryMessage {
				break
			}
			msgs := unpack(data)
			for _, msg := range msgs {
				if w.events[msg.id] != nil {
					w.events[msg.id](conn, msg.data)
				}
			}
		}
	}()
}
func (w *wsserver) Listen(mAddr, cAddr string) error {
	go func() {
		http.HandleFunc(cAddr, w.handler)
		err := http.ListenAndServe(mAddr, nil)
		panic(err)
	}()
	<-time.NewTicker(time.Nanosecond).C
	return nil
}
func (w *wsserver) OnOpen(f func(conn *websocket.Conn, req *http.Request)) {
	w.onopen = f
}
func (w *wsserver) OnClose(f func(conn *websocket.Conn)) {
	w.onclose = f
}
func (w *wsserver) OnEvent(id int32, f func(conn *websocket.Conn, data []byte)) {
	w.events[id] = f
}
func (w *wsserver) Emit(conn *websocket.Conn, id int32, data []byte) {
	w.Lock()
	defer w.Unlock()
	db := pack(msg{
		id:   id,
		data: data,
	})
	conn.WriteMessage(websocket.BinaryMessage, db)
}
