package socket

import (
	"crypto/tls"
	"net"
)

type TCPServerConfig struct {
	ReadBufferSize int
	TLS            *tls.Config
}

var defualt_tcp_server_config TCPServerConfig = TCPServerConfig{
	ReadBufferSize: 1024,
}

type TCPServer interface {
	Listen(network string, address string) error
	OnEvent(id int32, f func(conn net.Conn, data []byte))
	OnOpen(func(conn net.Conn))
	OnClose(func(conn net.Conn))
	Emit(conn net.Conn, id int32, data []byte)
}

func NewTCPServer(config ...TCPServerConfig) TCPServer {
	if len(config) == 0 {
		config = append(config, defualt_tcp_server_config)
	}
	return &tcpserver{
		config: config[0],
		events: make(map[int32]func(conn net.Conn, data []byte)),
	}
}

type tcpserver struct {
	config  TCPServerConfig
	onopen  func(conn net.Conn)
	onclose func(conn net.Conn)
	events  map[int32]func(conn net.Conn, data []byte)
}

func (t *tcpserver) Listen(network string, address string) error {
	var lis net.Listener
	var err error
	if t.config.TLS != nil {
		lis, err = tls.Listen(network, address, t.config.TLS)
	} else {
		lis, err = net.Listen(network, address)
	}
	if err != nil {
		return err
	}
	go func() {
		for {
			conn, err := lis.Accept()
			if err != nil {
				continue
			}
			go func() {
				defer func() {
					if t.onclose != nil {
						t.onclose(conn)
					}
					conn.Close()
				}()
				if t.onopen != nil {
					t.onopen(conn)
				}
				buf := make([]byte, t.config.ReadBufferSize)
				for {
					n, err := conn.Read(buf)
					if err != nil {
						break
					}
					data := buf[:n]
					msgs := unpack(data)
					for _, msg := range msgs {
						if t.events[msg.id] != nil {
							t.events[msg.id](conn, msg.data)
						}
					}
				}
			}()
		}
	}()
	return err
}

func (t *tcpserver) OnOpen(f func(conn net.Conn)) {
	t.onopen = f
}

func (t *tcpserver) OnClose(f func(conn net.Conn)) {
	t.onclose = f
}

func (t *tcpserver) OnEvent(id int32, f func(conn net.Conn, data []byte)) {
	t.events[id] = f
}

func (t *tcpserver) Emit(conn net.Conn, id int32, data []byte) {
	db := pack(msg{
		id:   id,
		data: data,
	})
	conn.Write(db)
}
